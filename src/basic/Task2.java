package basic;

public class Task2 {
	public static void main() {
		System.out.println(reversNumber(717) == 717);
		System.out.println(reversNumber(411) == 114);
		System.out.println(reversNumber(517) == 715);
		System.out.println(reversNumber(240) == 42);
		System.out.println(reversNumber(900) == 9);
		System.out.println(reversNumber(333) == 333);
		System.out.println("Finish");
	}

	static int reversNumber(int x) {
		assert(x<1000);
		assert(x>=100);
		return x;
	}
}
