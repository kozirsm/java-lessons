package basic;

public class Task4 {
	public static void main() {
		System.out.println(getMinutes( 60 ) ==  1 );
		System.out.println(getMinutes( 1800 ) ==  30 );
		System.out.println(getMinutes( 3600 ) ==  0 );
		System.out.println(getMinutes( 5400 ) ==  30 );
		System.out.println(getMinutes( 46683 ) ==  58 );
		System.out.println(getMinutes( 35841 ) ==  57 );
		System.out.println(getMinutes( 29575 ) ==  12 );
		System.out.println(getMinutes( 12284 ) ==  24 );
		System.out.println(getMinutes( 47008 ) ==  3 );
		System.out.println(getMinutes( 25303 ) ==  1 );
		System.out.println(getMinutes( 48554 ) ==  29 );
		System.out.println(getMinutes( 40585 ) ==  16 );
		System.out.println(getMinutes( 37407 ) ==  23 );
		System.out.println(getMinutes( 37594 ) ==  26 );
		System.out.println(getMinutes( 36014 ) ==  0 );
		System.out.println(getMinutes( 25023 ) ==  57 );
		System.out.println(getMinutes( 9083 ) ==  31 );
		System.out.println(getMinutes( 28950 ) ==  2 );
		System.out.println(getMinutes( 14519 ) ==  1 );
		System.out.println(getMinutes( 18581 ) ==  9 );
		System.out.println(getMinutes( 2163 ) ==  36 );
		System.out.println(getMinutes( 47313 ) ==  8 );
		System.out.println(getMinutes( 3678 ) ==  1 );
		System.out.println(getMinutes( 45695 ) ==  41 );
		System.out.println("Finish");
	}

	static int getMinutes(int seconds) {
		return -1;
	}
}
