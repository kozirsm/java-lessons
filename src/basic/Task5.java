package basic;

public class Task5 {
	public static void main() {
		System.out.println(getTime(16896).equals("4:41:36"));
		System.out.println(getTime(23724).equals("6:35:24"));
		System.out.println(getTime(3030).equals("0:50:30"));
		System.out.println(getTime(20223).equals("5:37:3"));
		System.out.println(getTime(5764).equals("1:36:4"));
		System.out.println(getTime(44181).equals("12:16:21"));
		System.out.println(getTime(17449).equals("4:50:49"));
		System.out.println(getTime(299).equals("0:4:59"));
		System.out.println(getTime(10829).equals("3:0:29"));
		System.out.println(getTime(42430).equals("11:47:10"));
		System.out.println(getTime(32743).equals("9:5:43"));
		System.out.println(getTime(46566).equals("12:56:6"));
		System.out.println(getTime(1011).equals("0:16:51"));
		System.out.println(getTime(25454).equals("7:4:14"));
		System.out.println(getTime(430).equals("0:7:10"));
		System.out.println(getTime(14839).equals("4:7:19"));
		System.out.println(getTime(34575).equals("9:36:15"));
		System.out.println(getTime(27581).equals("7:39:41"));
		System.out.println(getTime(29945).equals("8:19:5"));
		System.out.println(getTime(3626).equals("1:0:26"));
		System.out.println("Finish");
	}

	static String getTime(int secs) {
		return "" + getHours(secs) + ":" + getMinutes(secs) + ":" + getSeconds(secs);
	}

	static int getHours(int seconds) {
		return -1;
	}
	static int getMinutes(int seconds) {
		return -1;
	}
	static int getSeconds(int seconds) {
		return -1;
	}
}
