package arrays;

import java.util.Arrays;

public class Task2 {
	public static void main(String args[]) {
		int case1[] = new int[] {0, 1, -1, 3, -5};
		System.out.println( Arrays.equals(getSequence(5, 1), case1));

		int case2[] = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		System.out.println( Arrays.equals(getSequence(11, 0), case2));

		int case3[] = new int[] {0, 3, -3, 9, -15, 33, -63, 129, -255, 513};
		System.out.println( Arrays.equals(getSequence(10, 3), case3));

		System.out.println( getSequence(0, 1).length == 0);

		int case5[] = new int[] {0, -1, 1, -3, 5};
		System.out.println( Arrays.equals(getSequence(5, -1), case5));

		int case6[] = new int[] {0, 2, -2, 6, -10, 22, -42, 86, -170, 342, -682, 1366, -2730, 5462, -10922, 21846, -43690, 87382, -174762, 349526, -699050, 1398102, -2796202, 5592406, -11184810, 22369622, -44739242, 89478486, -178956970, 357913942};
		System.out.println( Arrays.equals(getSequence(30, 2), case6));

		System.out.println(getSequence(-5, 40).length == 0);

		int case8[] = new int[] {0, 8, -8, 24, -40, 88, -168, 344, -680, 1368, -2728, 5464};
		System.out.println( Arrays.equals(getSequence(12, 8), case8));

		System.out.println("Finish");
	}

	static int[] getSequence(int N, int a) {
		return new int[0];
	}
}
