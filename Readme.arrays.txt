== ������� ==

=== Task1 ===
����� ����� ������� �������� � ������� �����, ������� ������������� �� ".txt" (��� ����� ��������).
������� "getTxtIndex" ������ ������� ����� �������� (������� � 0) ��� -1 ���� ����� ������� �� ������.

=== Task2 ===
��������� ������ N ��������� ������������������ S:

S[0] = 0
S[i] = a - 2*S[i-1]

������� getSequence(int N, int a) ������ ������� ������ (�������� ������) ����� N.

=== Task3 ===
����� ����� ������������� � ������������ ��������� �������.

=== Task4 ===
���������� ����� ����� �� ����. ������� "reversed" ������ ������� *�����* ������, � ������� �������� ���� � �������� �������.

=== Task5 ===
���������� ��� ������ � ������� ����� ������ ';'.

=== Task6 ===
��������� ������ ���������� �������� �����. � �������������� ������� ����� ������ ������� �����, � ������ ��������� ����� ����� ������ 0.
��������, ������ ���������� ��� 20 �����: 1, 2, 3, 0, 5, 0, 7, 0, 0, 0, 11, 0, 13, 0, 0, 0, 17, 0, 19, 0.

=== Task7 ===
�������� ������� ���������������� ������������� �������.

=== Task8 (*) ===
����: ������� ����� �� ������ ������� ������. �� ���� ������� ������ �� ������ ���� ������. �� ���� ������ N ������ (N>2), �������������  � ���� ���. �� ��������� ����� ����� �������� ������. ������� ����� ������� �� 2, 3 ��� 5 ������ ����� � ������ �����. ����������� ������� ���������� ������ ��������� �������� ������� �� ������ (��� ����������� �������, ����� ������ ��� ������).
